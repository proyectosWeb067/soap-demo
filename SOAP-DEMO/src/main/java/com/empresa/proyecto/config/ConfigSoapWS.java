package com.empresa.proyecto.config;

import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;

@EnableWs
@Configuration
public class ConfigSoapWS {

	
	
	
	@Bean
	public XsdSchema usuarioSchema() {
		return new SimpleXsdSchema(new ClassPathResource("usuario.xsd"));
	}
	
	@Bean(name = "usuarios")
	public DefaultWsdl11Definition defaultWsdl11Definition(XsdSchema usuarioSchema) {
		DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
		wsdl11Definition.setPortTypeName("UsuariosPort");
		wsdl11Definition.setLocationUri("/soapws");
		wsdl11Definition.setTargetNamespace("http://empresa.com/usuarios-ws");
		wsdl11Definition.setSchema(usuarioSchema);
		return wsdl11Definition;
	}
	

	@Bean
	public ServletRegistrationBean mensajeDispatcherServlet(ApplicationContext applicationContext) {
		MessageDispatcherServlet servlet = new MessageDispatcherServlet();
		servlet.setApplicationContext(applicationContext);
		servlet.setTransformWsdlLocations(true);
		return new ServletRegistrationBean(servlet,"/soapws/*");
	}
	
}
