package com.empresa.proyecto.endpoint;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.empresa.proyecto.service.IUsuarioService;
import com.empresa.usuario_ws.ActualizarUsuarioRequest;
import com.empresa.usuario_ws.ActualizarUsuarioResponse;
import com.empresa.usuario_ws.AgregarUsuarioRequest;
import com.empresa.usuario_ws.AgregarUsuarioResponse;
import com.empresa.usuario_ws.EliminarUsuarioRequest;
import com.empresa.usuario_ws.EliminarUsuarioResponse;
import com.empresa.usuario_ws.EstadoServicio;
import com.empresa.usuario_ws.GetUsuarioPorIdRequest;
import com.empresa.usuario_ws.GetUsuarioPorIdResponse;
import com.empresa.usuario_ws.GetUsuariosResponse;
import com.empresa.usuario_ws.Usuario; 

@Endpoint
public class UsuarioEndPoint {

	private static final String NAME_SPACE ="http://empresa.com/usuario-ws";
	
	@Autowired
	private IUsuarioService iUsuario;
	
	@PayloadRoot(namespace=NAME_SPACE, localPart="getUsuariosPorIdRequest")
	@ResponsePayload
	public GetUsuarioPorIdResponse obtenerUsuarioPorId(@RequestPayload GetUsuarioPorIdRequest request) {
		GetUsuarioPorIdResponse response = new GetUsuarioPorIdResponse();
		 Usuario usuario = new Usuario();
		 BeanUtils.copyProperties(iUsuario.obtenerUsuarioPorId(request.getIdEmpleado()),usuario);
		 response.setUsuario(usuario);
		return response;
	}
	
	@PayloadRoot(namespace=NAME_SPACE, localPart="getUsuariosRequest")
	@ResponsePayload
	public GetUsuariosResponse obtenerUsuarios() {
		GetUsuariosResponse response = new GetUsuariosResponse();
		 List<Usuario> listaUsuarios = new ArrayList<>();
		 List<com.empresa.proyecto.model.Usuario> usuariosEntity = iUsuario.getUsuarios();
		for (int i = 0; i < usuariosEntity.size(); i++) {
			Usuario usuario = new Usuario();
			 BeanUtils.copyProperties(usuariosEntity.get(i),usuario);
			 listaUsuarios.add(usuario);
		}
		response.getUsuario().addAll(listaUsuarios);
		return response; 
	}
	
	@PayloadRoot(namespace = NAME_SPACE, localPart="agregarUsuarioRequest")
	@ResponsePayload
	public AgregarUsuarioResponse agregarUsuario(@RequestPayload AgregarUsuarioRequest request) {
		AgregarUsuarioResponse response = new AgregarUsuarioResponse();
		com.empresa.proyecto.model.Usuario usuario = new com.empresa.proyecto.model.Usuario();
		EstadoServicio estado = new EstadoServicio();
		usuario.setNombre(request.getNombre());
		usuario.setApellido(request.getApellido());
		usuario.setCedula(request.getCedula()); 
		usuario.setSalario(request.getSalario());
		
		boolean guardarUsuario = iUsuario.agregarUsuario(usuario);
		if(guardarUsuario == false) {
			estado.setCodigoEstado("ERROR");
			estado.setMensaje("Se produjo un error al agregar. ");
			response.setEstadoServicio(estado);
		}else{
			Usuario usuarioInformacion = new Usuario();
			 BeanUtils.copyProperties(usuario, usuarioInformacion);
			response.setUsuario(usuarioInformacion);
			estado.setCodigoEstado("EXITO");
			estado.setMensaje("Se agrego el usuario satisfactoriamente. ");
			response.setEstadoServicio(estado);
		}
		
		return response;
	}
	
	@PayloadRoot(namespace=NAME_SPACE, localPart="actualizarUsuarioRequest")
	@ResponsePayload
	public ActualizarUsuarioResponse actualizarUsuario(@RequestPayload ActualizarUsuarioRequest request) {
		ActualizarUsuarioResponse response = new ActualizarUsuarioResponse();
		com.empresa.proyecto.model.Usuario usuarioEntity = new com.empresa.proyecto.model.Usuario(); 
		BeanUtils.copyProperties(request.getUsuario(),usuarioEntity);
		iUsuario.actualizarUsuario(usuarioEntity);
		EstadoServicio estado = new EstadoServicio();
		estado.setCodigoEstado("EXITO");
		estado.setMensaje("Se actualizo satisfactoriamente."); 
		response.setEstadoServicio(estado);
		return response;
	}
	
	@PayloadRoot(namespace= NAME_SPACE, localPart="eliminarUsuarioRequest")
	@ResponsePayload
	public EliminarUsuarioResponse eliminarUsuario(@RequestPayload EliminarUsuarioRequest request) {
		EliminarUsuarioResponse response = new EliminarUsuarioResponse();
		EstadoServicio estado = new EstadoServicio();
		com.empresa.proyecto.model.Usuario usuario = iUsuario.obtenerUsuarioPorId(request.getIdUsuario());
		if(usuario !=null) { 
			estado.setCodigoEstado("EXITO");
			estado.setMensaje("Se elimino el registro satisfactoriamente.");
			 iUsuario.eliminarUsuario(usuario);
		}else {
			estado.setCodigoEstado("ERROR");
			estado.setMensaje("No se pudo realizar la operación.");
		} 
		
		return response;
	}
}
