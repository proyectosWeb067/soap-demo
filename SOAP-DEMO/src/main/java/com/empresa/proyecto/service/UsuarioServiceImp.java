package com.empresa.proyecto.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.empresa.proyecto.model.Usuario;
import com.empresa.proyecto.repository.UsuarioRepository;

@Service
public class UsuarioServiceImp implements IUsuarioService {

	@Autowired
	private UsuarioRepository usuarioReposiory;

	@Override
	public Usuario obtenerUsuarioPorId(long id) {
		Usuario usuario = usuarioReposiory.findById(id).get();
		return usuario;
	}

	@Override
	public List<Usuario> getUsuarios() {
		List<Usuario> usuarios = usuarioReposiory.findAll();
		return usuarios;
	}

	@Override
	public synchronized boolean agregarUsuario(Usuario usuario) {

		Usuario usuarioAgregado = usuarioReposiory.save(usuario);

		return usuarioAgregado != null ? true : false;
	}

	@Override
	public void eliminarUsuario(Usuario usuario) {
		   usuarioReposiory.delete(usuario); 
	}

	@Override
	public void actualizarUsuario(Usuario usuario) {
		
		usuarioReposiory.save(usuario);
	}

}
