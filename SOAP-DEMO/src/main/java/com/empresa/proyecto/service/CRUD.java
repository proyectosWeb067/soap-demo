package com.empresa.proyecto.service;

import java.util.List;

public interface CRUD<T> {

	T obtenerUsuarioPorId(long id);
	List<T> getUsuarios();
	boolean agregarUsuario(T t);
	void eliminarUsuario(T t);
	void actualizarUsuario(T t); 
}
